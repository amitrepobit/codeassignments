# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts
yarn start (To start the application)
yarn test (To start running test scripts and snapshots, Enzyme and Jest used in the application.)

## Versions
React 17 with enzyme-adapter-react-16 is used, Ideally enzyme-adapter-react-17 must be used with React 17, but seems its not yet stable.
Styles components versions 5.2 is used in the application.


