/*
 * Constants file
 * - All contants in the application should go here
 */
const API = {
    TimeZoneAPI: 'http://api.timezonedb.com/v2.1/list-time-zone?key=XWSLLPX5RMIZ&format=json&zone=Europe/*',
    TimeAPI:'http://api.timezonedb.com/v2/get-time-zone?key=XWSLLPX5RMIZ&format=json&by=zone&zone='
    }
  
  export default API;