/*
 * The SERVICE API Helpers file. 
 This element is made common, hence can be reused.
 */

import axios from 'axios';

export const get = endpoint => {
  return axios({
    method: 'GET',
    url: endpoint,
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(function (res) {
      if (res.status < 200 || res.status > 302) {
        return Promise.reject(new Error('Something Went Wrong!'));
      }
      return res.data;
    })
    .catch(function (error) {
      return error
    });
}
