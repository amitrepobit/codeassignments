import {get} from "../../src/utils/serviceApis";
import API from "../constants";

export const getTimeZones = {
    retrieveTimeZone : ()=>{ 
        return get(`${API.TimeZoneAPI}`).then(response => {
            return response.zones;
        });
    } ,
    retrieveSelZoneTime : (zone)=>{
        return get(`${API.TimeAPI}${zone}`).then(response => {
            return response.formatted;
        });
    }
}