import React from "react";
import { CardComp } from "./CardsComp"

const UiPlaceHolderComp = () => {// Its just the placeholder to show the CardComp
    return (

        <div className="container">
            <CardComp />
        </div>
    );

}
export default UiPlaceHolderComp;
