import styled from "styled-components";
import { Icon, WarninigIcon, ChevronIcon } from "../assets/icon";

export const StyledButton = styled.button`
  font-size:15px;
  font-weight:regular;
  height:40px;
  padding-left:10px;
  padding-right:10px;     
`;

export const SecondaryCTA = styled(StyledButton)`
color: #FFFFFF;
background-color:#4D6474;
border:none; 
&:hover {
    background-color:#3E505C;
  }
  &:focus{
    background-color:#31404A;
  }
`


export const TertiaryCTA = styled(StyledButton)`
color: #252525;
background-color:#FFFFFF;
border:1px solid #252525;
&:hover {
    background-color:rgb(0,0,0,7%);    
  }
  &:focus{
    background-color:rgba(0,0,0,15%);    
}
`
export const StyledDivOuter = styled.div`
display:flex;
flex-direction:column;
border-top:1px solid #252525;
border-bottom:1px solid #252525;
`
export const StyledDivRow = styled.div`
display:flex;
flex-direction:row;
`
export const StyledDivVertical = styled.div`
display:flex;
flex-direction:column;
justify-content:center;
`
export const StyledDivVerticalLeft = styled.div`
display:flex;
flex-direction:column;
margin-right:auto;
`
export const StyledLabelLarge = styled.label`
font-size:22px;
font-weight:medium;
color:#252525;
line-height:22px;
margin-bottom:0px;
`
export const StyledLabelSmall = styled.label`
font-size:15px;
font-weight:bold;
color:#252525;
line-height:15px;
margin-bottom:0px;
margin-top:2px;
`
export const StyledLabelMediumWeight = styled.label`
font-size:15px;
font-weight:light;
color:#252525;
line-height:15px;
margin-bottom:0px;
`
export const StyledSeparator = styled.div`
display:flex;
width:1px;
border:1px solid #252525;
background-color:black;
opacity:34%;
justify-content:center;
height:35px;

`

export const CardComp = () => {// Created based componenet 'button and then extended it using styled component'

    return (

        <StyledDivOuter>
            <StyledDivRow>
                <StyledDivVerticalLeft style={{ paddingTop: "17px", paddingBottom: "20px" }}>
                    <StyledLabelLarge>Cancel / recall payment, GBP 1,000.00</StyledLabelLarge>
                    <StyledLabelSmall>1234567890123456 (GB), KUIML Business Company</StyledLabelSmall>
                </StyledDivVerticalLeft>
                <StyledDivVertical style={{ paddingTop: "15px" }}>
                    <StyledDivRow>
                        <div style={{ display: "flex", padding: "5px 0 5px 0" }}>
                            <div style={{ display: "flex", marginRight: "20px", marginLeft: "20px" }}>
                                <Icon />
                            </div>
                            <StyledSeparator />
                            <div style={{ display: "flex", marginRight: "20px", marginLeft: "20px" }}>
                                <Icon />
                            </div>
                        </div>
                        <TertiaryCTA style={{ marginRight: "10px" }}>Reject</TertiaryCTA>
                        <SecondaryCTA>Authorise</SecondaryCTA>
                    </StyledDivRow>
                </StyledDivVertical>
            </StyledDivRow>
            <StyledDivRow style={{ paddingBottom: "18px" }}>
                <StyledSeparator />
                <StyledDivVertical style={{ paddingLeft: "10px", paddingRight: "25px" }}>
                    <StyledLabelMediumWeight>Request reference
                    </StyledLabelMediumWeight>
                    <StyledLabelSmall>SET29383ABCH
                    </StyledLabelSmall>
                </StyledDivVertical>
                <StyledSeparator />
                <StyledDivVertical style={{ paddingLeft: "10px", paddingRight: "25px" }}>
                    <StyledLabelMediumWeight>Category
                    </StyledLabelMediumWeight>
                    <StyledLabelSmall>Payment
                    </StyledLabelSmall>
                </StyledDivVertical>
                <StyledSeparator />
                <StyledDivVertical style={{ paddingLeft: "10px", paddingRight: "25px" }}>
                    <StyledLabelMediumWeight>Request status
                    </StyledLabelMediumWeight>
                    <StyledDivRow><WarninigIcon />
                        <StyledLabelSmall style={{ marginLeft: "6px", marginTop: "0px" }}>Pending authorisation
                    </StyledLabelSmall>
                    </StyledDivRow>
                </StyledDivVertical>
                <StyledDivVertical style={{ marginLeft: "auto", justifyContent: "flex-end" }}>
                    <StyledDivRow>
                        <StyledLabelSmall style={{ textDecoration: "underline" ,marginTop:"0px",marginRight:"2px"}}>Full Details
                    </StyledLabelSmall><ChevronIcon />
                    </StyledDivRow>
                </StyledDivVertical>
            </StyledDivRow>
        </StyledDivOuter>
    );
}


