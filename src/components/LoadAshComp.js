import React from "react";

const LoadAshComp = () => { // component to show the _once related functionality. Browser console shall be checked for the output
    
    function add(x, y) {
        return x + y;
    }
    function once(fn) {
        var result;
        return function () {
            if (fn) { // As fn is available very first time hence condition will execute and then it makes fn as null, hence second time it will just return the previous result.
                result = fn.apply(this, arguments);
                fn = null;
            }

            return result;
        }

    }
    let onceAdd = once(add); // creates the object of once

    return (
        <>
            <h1>Summed value is can be seen in console</h1>
            <br/>
            <button onClick={() => { console.log(onceAdd(2, 3)) }} >Parameters 2,3</button>
            <button onClick={() => { console.log(onceAdd(4, 5)) }} >Parameters 4,5</button>

        </>
    );
}

export default LoadAshComp;