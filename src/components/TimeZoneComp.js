import React, { useState } from "react"
import { getTimeZones } from "../utils/serviceHelper"


const TimeZoneComp = () => { // Component to present the zone vs time requirement.

    const [timeZoneList, setTimeZoneList] = useState([]);
    const [selectedZone, setZoneSelection] = useState("");
    const [selectedZoneTime, setSelectedTime] = useState("");

    const [counter, setCounter] = useState(0);
    const [timerFlag, setTimerFlag] = useState(Object);

    const handleChange = (event) => { // change handler of zone list droppdown.
        clearTimeout(timerFlag); // cleared previous timer on the new zone selection
        setZoneSelection(event.target.value); // can be further drilled by destructring {target}, but left it as is for now.
    }

    const populateTimeZones = () => {
        getTimeZones.retrieveTimeZone().then(response => {
            setTimeZoneList(response)
        });
    }

    React.useEffect(() => {// It should load one list of zones of component mount
        populateTimeZones();
    }, [])

    React.useEffect(() => {// It should load the zone details including required time , depends on selectedZone and counter maintained to refetch the time after 5 sec
        if (selectedZone) {
            getTimeZones.retrieveSelZoneTime(selectedZone).then(zoneTiming => {
                setTimerFlag(setTimeout(() => (setCounter(Math.random())), 5000))
                setSelectedTime(zoneTiming);
            })
        }
        return () => {
            clearTimeout(timerFlag); // clearing the timer on unMount
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedZone, counter]);

    return (
        <>
            <form>
                <div className="container">
                    <div className="row">
                        <div id="selectedZone" className="col-3 Time-container">{selectedZone ? selectedZone : "Selected zone"}</div>
                        <div id="selectedTime" className="col-3 Time-container">{selectedZoneTime ? selectedZoneTime.split(" ")[1] : "Selected zone time"}</div>
                        <div className="col-6- Time-list-container">
                            <select className="form-control form-control-lg" onChange={handleChange}>
                                <option value="">Select time zone</option>
                                {timeZoneList && timeZoneList.map((value, index) => {
                                    return (<option key={index} value={value.zoneName}>{value.zoneName}</option>)
                                })}

                            </select>
                        </div>
                    </div>

                </div>
            </form>
        </>
    )

}
export default TimeZoneComp;