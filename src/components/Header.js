/*
 * Header Component.
 * - Load using <Header />.
 * 
 * A dumb component which provides asscess  to other areas of the application.
 */
import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <nav className="navbar navbar-expand-sm bg-primary navbar-dark HeaderNav">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to="/">
                        Time zone
                </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/once">
                        Loadsh
                </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/card">
                        UI
                </Link>
                </li>
            </ul>
        </nav >
    );
}


export default Header;
