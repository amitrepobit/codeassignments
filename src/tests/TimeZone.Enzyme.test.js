import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import React from "react";
import TimeZoneComp from "../components/TimeZoneComp";
import { shallow } from "enzyme";
import { getTimeZones } from "../utils/serviceHelper";
import 'regenerator-runtime/runtime';
configure({ adapter: new Adapter() });
jest.mock("../utils/serviceHelper");

function renderTimeZoneForm() {
    const configureDefaultProps = {
        populateTimeZones: () => { }
    }

    const props = { ...configureDefaultProps };
    return shallow(<TimeZoneComp {...props} />);
}

describe("Timezone component suite", () => {
    it("Form element should be mounted", () => {
        const wrapper = renderTimeZoneForm();
        expect(wrapper.find('form').length).toBe(1);
    });
    it("Selected zone container must be present", () => {
        const wrapper = renderTimeZoneForm();
        expect(wrapper.find('#selectedZone').text()).toEqual("Selected zone");
        expect(wrapper.find('#selectedTime').text()).toEqual("Selected zone time");

    });
});

describe("Time zone data loading test suite", () => {
    const wrapper = renderTimeZoneForm();
    it('Test list zones service call:::::::Success', () => {
        const res = {
            zones: [{
                zoneName: "Europe/Andorra"
            },
            {
                zoneName: "Europe/Tirane"
            }]
        };

        getTimeZones.retrieveTimeZone.mockImplementation(() => Promise.resolve(res.zones));

        jest.spyOn(React, 'useEffect').mockImplementationOnce(f => f());
        setTimeout(() => {
            expect(wrapper.find("option").length).toBe(3);
        }, 500)
    });
    it('Test list zones service call:::::::Fail', () => {
        const res = {
            zones: []
        };

        getTimeZones.retrieveTimeZone.mockImplementation(() => Promise.resolve(res.zones));

        jest.spyOn(React, 'useEffect').mockImplementationOnce(f => f());
        setTimeout(() => {
            expect(wrapper.find("option").length).toBe(1);
        }, 500)
    });
})

describe("Selected zone time test suite", () => {
    const wrapper = renderTimeZoneForm();
    it("Selected zone's time should be correct", () => {
        const res = {
            formatted: "2021-01-10 13:30:49",
            zoneName: "Europe/Brussels"
        };
        let event = {
            target: {
                value: "Europe/Brussels"
            }
        };
        getTimeZones.retrieveSelZoneTime.mockImplementation(() => Promise.resolve(res.formatted));
        wrapper.find("select").simulate("change", event);
        setTimeout(() => {
            expect(wrapper.find("selectedTime").text).toBe("13:30:49");
        }, 500)

    });
})