import React from "react";
import TimeZoneComp from "../components/TimeZoneComp";
import renderer from "react-test-renderer";

describe("Test snapshot of the component",()=>{
    it("All elements must be present on the fome",()=>{
        const tree = renderer.create(<TimeZoneComp/>);
        expect(tree).toMatchSnapshot();
    })
});
