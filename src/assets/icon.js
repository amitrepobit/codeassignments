import React from "react";

export const Icon = () => {
    return (
        <svg style={{ width: "1.25rem" }} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" className="svg-inline--fa fa-file fa-w-12 fa-fw fa-2x" ><path fill="currentColor" d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm160-14.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z"></path></svg>
    );

}

export const WarninigIcon = () => {
    return (
        // <svg width="15" height="15" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        //     <circle cx="12" cy="12" r="11" fill="#0074CC" stroke="#0074CC" strokeWidth="2" />
        //     <rect x="10.5" y="10" width="3" height="9" fill="white" />
        //     <circle cx="12" cy="6.5" r="1.5" fill="white" />
        // </svg>
        <svg width="15" height="15" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="12" cy="12" r="11" fill="#DC5133" stroke="#DC5133" strokeWidth="2" />
			<path d="M12.5 14L11.5 14L10.5 5L13.5 5L12.5 14Z" fill="white" />
			<circle cx="12" cy="17.5" r="1.5" transform="rotate(-180 12 17.5)" fill="white" />
		</svg>
    );

}

export const ChevronIcon = () => {
    return (
        <svg width="15" height="15" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path
				d="M10.2077 9.9299L10.885 10.6656L11.7127 9.90354L10.8589 9.17097L10.2077 9.9299ZM1.72014 1.32982C1.301 0.970183 0.669673 1.01843 0.31004 1.43757C-0.0495926 1.85672 -0.0013482 2.48804 0.417797 2.84767L1.72014 1.32982ZM0.671826 17.3506C0.265532 17.7247 0.239426 18.3573 0.613517 18.7636C0.987608 19.1699 1.62023 19.196 2.02653 18.822L0.671826 17.3506ZM10.8589 9.17097L1.72014 1.32982L0.417797 2.84767L9.55652 10.6888L10.8589 9.17097ZM9.53034 9.19424L0.671826 17.3506L2.02653 18.822L10.885 10.6656L9.53034 9.19424Z"
				fill="black"
			/>
		</svg>
    );

}