import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import TimeZoneComp from "./components/TimeZoneComp";
import LoadAshComp from "./components/LoadAshComp";
import UiPlaceHolderComp from "./components/UiCompPlaceHolder";
import Header from "./components/Header"
import {
  Route,
  Redirect,
  Switch,
  BrowserRouter
} from "react-router-dom";


function App() {
  return (
    <BrowserRouter>
        <Header>
        </Header>
        <Switch>
          <Route exact path="/" component={TimeZoneComp} />
          <Route exact path="/once" component={LoadAshComp} />
          <Route exact path="/card" component={UiPlaceHolderComp} />
          <Redirect from="*" to="/" />
        </Switch>
      </BrowserRouter>
    
  );
}

export default App;
