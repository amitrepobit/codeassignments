/*
 * Babel config file.
 * - This files has all babel configurations.
 */
module.exports = function (api) {
    api.cache(true);
  
    const presets = [
      "@babel/preset-env",
      "@babel/preset-react"
    ];
    const env = {
      "production": {
        "presets": ["minify"]
      }
    };
  
    return {
      presets,
      env
    };
  }